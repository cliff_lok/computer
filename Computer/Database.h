//
//  Database.h
//  Computer
//
//  Created by E-Liang Tan on 16/11/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#ifndef Computer_Database_h
#define Computer_Database_h

#include "Constants.h"
#include "DatabaseEntity.h"

#include <vector>

AI_NAMESPACE_BEGIN

// Must be able to cope with requests from multiple maps running in different threads
class Database {
public:
	virtual void connectToDatabase();
	virtual void closeConnectionToDatabase();
	
//	virtual std::vector<Concept *> getAllConcepts();
//	virtual std::vector<Concept *> getConceptsWithIDs(int ids[]);
//	virtual std::vector<ConceptInstance *> getAllConceptInstances();
//	virtual std::vector<ConceptInstance *> getConceptInstancesWithIDs(int ids[]);
//	virtual std::vector<ConceptInstance *> getConceptInstancesWithInstantiatedConceptID(int conceptID);
//	virtual std::vector<ConceptLinker *> getAllConceptLinkers();
//	virtual std::vector<ConceptLinker *> getConceptLinkersWithIDs(int ids[]);
//	virtual std::vector<ConceptLinker *> getConceptLinkersWithOriginatingID(int conceptID, ConceptLinkerType linkerType);
//	virtual std::vector<ConceptLinker *> getConceptLinkersWithTargetID(int conceptID, ConceptLinkerType linkerType);
//	virtual std::vector<ConceptLinker *> getConceptLinkersWithRepresentedConceptID(int conceptID);
	virtual void getConceptsWithConceptOfFocus(Concept *conceptOfFocus, std::vector<Concept *> concepts, std::vector<ConceptInstance *> conceptInstances, std::vector<ConceptLinker *> conceptLinkers);
	
//	virtual std::vector<ConceptNaturalLanguageRepresenter *> getNaturalLanguageRepresenters();
//	virtual std::vector<ConceptNaturalLanguageRepresenter *> getNaturalLanguageRepresentersWithIDs(int ids[]);
//	virtual std::vector<ConceptNaturalLanguageRepresenter *> getNaturalLanguageRepresentersWithLanguage(std::string languageCode);
//	virtual std::vector<ConceptNaturalLanguageRepresenter *> getNaturalLanguageRepresentersWithRoughPronunciation(U_ICU_NAMESPACE::UnicodeString roughPronunciation);
//	virtual std::vector<ConceptNaturalLanguageRepresenter *> getNaturalLanguageRepresentersWithString(U_ICU_NAMESPACE::UnicodeString string);
	
//	virtual std::vector<ConceptNaturalLanguageRepresentationLinker *> getNaturalLanguageRepresentationLinkers();
//	virtual std::vector<ConceptNaturalLanguageRepresentationLinker *> getNaturalLanguageRepresentationLinkersWithIDs(int ids[]);
//	virtual std::vector<ConceptNaturalLanguageRepresentationLinker *> getNaturalLanguageRepresentationLinkersWithNaturalLanguageRepresenterIDs(int ids[]);
//	virtual std::vector<ConceptNaturalLanguageRepresentationLinker *> getNaturalLanguageRepresentationLinkersWithRepresentedIDs(int ids[], ConceptLinkerType linkerType);
};

AI_NAMESPACE_END

#endif
