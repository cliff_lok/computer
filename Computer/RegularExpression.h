//
//  RegularExpression.h
//  Computer
//
//  Created by E-Liang Tan on 29/11/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#ifndef Computer_RegularExpression_h
#define Computer_RegularExpression_h

#include "Constants.h"

AI_NAMESPACE_BEGIN

class RegularExpression {
public:
	bool hasMatchesInString(const char *expression, const char *string, std::string **errorString);
};

AI_NAMESPACE_END

#endif
