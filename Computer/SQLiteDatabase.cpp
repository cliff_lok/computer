//
//  SQLiteDatabase.cpp
//  Computer
//
//  Created by E-Liang Tan on 16/11/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <map>
#include <vector>
#include "SQLiteDatabase.h"

AI_NAMESPACE_USE

SQLiteDatabase::SQLiteDatabase(const char *path) {
	filePath = path;
	database = NULL;
	std::string pathString = std::string(path);
	pathString += arc4random();
	isTransactionInProgress = false;
}

SQLiteDatabase::~SQLiteDatabase() {
	closeConnectionToDatabase();
}

void SQLiteDatabase::connectToDatabase() {
	if (database == NULL) {
		int errorCode = sqlite3_open(filePath, &database);
		if (errorCode != SQLITE_OK) {
			printf("SQLiteDatabase -connectToDatabase(): Well this sucks SQLite database couldn't open with code %d error: %s\n", errorCode, sqlite3_errmsg(database));
		}
	}
}

void SQLiteDatabase::closeConnectionToDatabase() {
	if (database != NULL) {
		int errorCode = sqlite3_close(database);
		if (errorCode != SQLITE_OK) {
			printf("SQLiteDatabase -closeConnectionToDatabase(): Well this sucks SQLite database couldn't close with code %d error: %s\n", errorCode, sqlite3_errmsg(database));
			return;
		}
		database = NULL;
	}
}

void SQLiteDatabase::beginTransaction() {
	connectToDatabase();
	char *errorMessage = NULL;
	performRequest("BEGIN IMMEDIATE TRANSACTION;", NULL, NULL, &errorMessage);
	if (errorMessage != NULL) {
		printf("SQLiteDatabase -beginTransaction(): Well this sucks SQLite database couldn't begin transaction with code %s\n", errorMessage);
		return;
	}
	isTransactionInProgress = true;
}

void SQLiteDatabase::endTransaction(bool closeConnection) {
	char *errorMessage = NULL;
	performRequest("COMMIT TRANSACTION;", NULL, NULL, &errorMessage);
	if (errorMessage != NULL) {
		printf("SQLiteDatabase -endTransaction(): Well this sucks SQLite database couldn't end transaction with code %s\n", errorMessage);
		return;
	}
	isTransactionInProgress = false;
	if (closeConnection == true) {
		closeConnectionToDatabase();
	}
}

void SQLiteDatabase::updateSchema(const char *filePath) {
	// DO NOT BEGIN TRANSACTION BEFORE ATTACHING DATABASE
	connectToDatabase();
	sqlite3_stmt *statement;
	char *zSQL = (char *)"";
	std::string SQLString = "";
	std::string cacheString = "";
	std::vector<char *> cacheStringVector;
	const char *cacheStringChar = cacheString.c_str();
	char *cacheStringToken;
	std::vector<char*>::iterator vit;
	int errorCode;
	
	zSQL = sqlite3_mprintf("ATTACH DATABASE '%q' AS 'schemaSpecifications';", filePath);
	sqlite3_prepare_v2(database, zSQL, -1, &statement, NULL);
	errorCode = sqlite3_step(statement);
	if (errorCode != SQLITE_DONE) {
		printf("SQLiteDatabase -updateSchema(): Well this sucks SQLite database couldn't attach table at %s with code %d error: %s\n", filePath, errorCode, sqlite3_errmsg(database));
	}
	sqlite3_finalize(statement);
	sqlite3_free(zSQL);
	
	// BEGIN TRANSACTION HERE
	beginTransaction();
	
	// Remove tables
	sqlite3_prepare_v2(database, "SELECT name FROM sqlite_master WHERE (type='table', name NOT IN (SELECT entity_name FROM schemaSpecifications.entities), name NOT IN (SELECT entity_name FROM schemaSpecifications.old_entity_names))", -1, &statement, NULL);
	while (sqlite3_step(statement) == SQLITE_ROW) {
		SQLString = "";
		SQLString += "DROP TABLE ";
		SQLString.append((char *)sqlite3_column_text(statement, 0));
		SQLString += ";";
		
		sqlite3_prepare_v2(database, SQLString.c_str(), -1, &statement, NULL);
		int errorCode = sqlite3_step(statement);
		if (errorCode != SQLITE_DONE) {
			printf("SQLiteDatabase -updateSchema(): Well this sucks SQLite database couldn't drop table (SQL: %s) with code %d error: %s\n", SQLString.c_str(), errorCode, sqlite3_errmsg(database));
		}
		sqlite3_finalize(statement);
	}
	
	// Rename tables (ALTER TABLE name RENAME TO name)
	SQLString = "";
	cacheString = "";
	sqlite3_prepare_v2(database, "SELECT entity_name FROM schemaSpecifications.entities WHERE (entity_name NOT IN (SELECT name FROM sqlite_master), entity_id IN (SELECT entity_id FROM schemaSpecifications.old_entity_specifications), entity_name IN (SELECT entity_name FROM schemaSpecifications.old_entity_specifications))", -1, &statement, NULL);
	while (sqlite3_step(statement) == SQLITE_ROW) {
		cacheString += "'";
		cacheString.append((char *)sqlite3_column_text(statement, 0));
		cacheString += "',";
	}
	sqlite3_finalize(statement);
	if (cacheString.length() > 0) {
		cacheString[cacheString.length()-1] = '\0';
		SQLString = "SELECT name FROM sqlite_master WHERE (type='table', name IN ('" + cacheString + "))";
		sqlite3_prepare_v2(database, SQLString.c_str(), -1, &statement, NULL);
		cacheString = "";
		while (sqlite3_step(statement) == SQLITE_ROW) {
			cacheString.append((char *)sqlite3_column_text(statement, 0));
			cacheString += ";";
		}
		sqlite3_finalize(statement);
		cacheStringVector.clear();
		cacheStringChar = cacheString.c_str();
		cacheStringToken = strtok((char *)cacheStringChar, ";");
		while (cacheStringToken != NULL) {
			if (strlen(cacheStringToken) > 0) {
				SQLString = "";
				
				zSQL = sqlite3_mprintf("SELECT entity_name FROM schemaSpecifications.entities WHERE (entity_id IN (SELECT entity_id FROM schemaSpecifications.old_entity_specifications WHERE entity_name='%s'))", cacheStringToken);
				sqlite3_prepare_v2(database, zSQL, -1, &statement, NULL);
				if (sqlite3_step(statement) == SQLITE_ROW && strlen((char *)sqlite3_column_text(statement, 0)) > 0) {
					SQLString += "ALTER TABLE '";
					SQLString.append(*vit);
					SQLString += "' RENAME TO '";
					SQLString.append((char *)sqlite3_column_text(statement, 0));
					SQLString += "';";
				}
				sqlite3_finalize(statement);
				sqlite3_free(zSQL);
				
				sqlite3_prepare_v2(database, SQLString.c_str(), -1, &statement, NULL);
				errorCode = sqlite3_step(statement);
				if (errorCode != SQLITE_DONE) {
					printf("SQLiteDatabase -updateSchema(): Well this sucks SQLite database couldn't alter table (SQL: %s) with code %d error: %s\n", SQLString.c_str(), errorCode, sqlite3_errmsg(database));
				}
				sqlite3_finalize(statement);
			}
			cacheStringToken = strtok(NULL, ";");
		}
	}
	
	// Add tables
	SQLString = "";
	cacheString = "";
	sqlite3_prepare_v2(database, "SELECT entity_name FROM schemaSpecifications.entities WHERE (entity_name NOT IN (SELECT name FROM sqlite_master))", -1, &statement, NULL);
	do {
		errorCode = sqlite3_step(statement);
		if (errorCode == SQLITE_ROW) {
			cacheString.append((char *)sqlite3_column_text(statement, 0)); // INSERT VALUES
			cacheString += ";";
		}
		else if (errorCode != SQLITE_DONE) {
			printf("SQLiteDatabase -updateSchema(): Well this sucks SQLite database couldn't do add tables operation 1 with code %d error: %s\n", errorCode, sqlite3_errmsg(database));
			return;
		}
	} while (errorCode == SQLITE_ROW);
	sqlite3_finalize(statement);
	if (cacheString.length() > 0) {
		cacheStringChar = cacheString.c_str();
		cacheStringToken = strtok((char *)cacheStringChar, ";");
		while (cacheStringToken != NULL) {
			if (strlen(cacheStringToken) > 0) {
				SQLString = "";
				SQLString += "CREATE TABLE ";
				SQLString.append(cacheStringToken);
				SQLString += "(";
				
				zSQL = sqlite3_mprintf("SELECT name, data_type FROM schemaSpecifications.entity_specifications WHERE (entity_id IN (SELECT entity_id FROM schemaSpecifications.entities WHERE entity_name='%s'))", cacheStringToken);
				sqlite3_prepare_v2(database, zSQL, -1, &statement, NULL);
				while (sqlite3_step(statement) == SQLITE_ROW && strlen((char *)sqlite3_column_text(statement, 0)) > 0) {
					SQLString.append((char *)sqlite3_column_text(statement, 0));
					SQLString += " ";
					SQLString.append((char *)sqlite3_column_text(statement, 1));
					SQLString += ",";
				}
				sqlite3_finalize(statement);
				sqlite3_free(zSQL);
				
				if (SQLString[SQLString.length() -1] == ',') {
					SQLString[SQLString.length() -1] = ')';
				}
				else if (SQLString[SQLString.length() -1] == '(') {
					SQLString += ')';
				}
				
				sqlite3_prepare_v2(database, SQLString.c_str(), -1, &statement, NULL);
				int errorCode = sqlite3_step(statement);
				if (errorCode != SQLITE_DONE) {
					printf("SQLiteDatabase -updateSchema(): Well this sucks SQLite database couldn't add table (SQL: %s) with code %d error: %s\n", SQLString.c_str(), errorCode, sqlite3_errmsg(database));
				}
				sqlite3_finalize(statement);
			}
			cacheStringToken = strtok(NULL, ";");
		}
	}
	
	endTransaction(false);
	
	sqlite3_prepare_v2(database, "DETACH DATABASE schemaSpecifications;", -1, &statement, NULL);
	errorCode = sqlite3_step(statement);
	if (errorCode != SQLITE_DONE) {
		printf("SQLiteDatabase -updateSchema(): Well this sucks SQLite database couldn't detach database with code %d error: %s\n", errorCode, sqlite3_errmsg(database));
	}
	sqlite3_finalize(statement);
	closeConnectionToDatabase();
}

void SQLiteDatabase::performRequest(const char *sql, int (*callback)(void *, int, char **, char **), void *context, char **errorMessage) {
	sqlite3_exec(database, sql, callback, context, errorMessage);
}

std::vector<Concept *> SQLiteDatabase::getConcepts(const char *sql) { 
	std::vector<Concept *> vector;
	
	bool wasDatabaseOpen = false;
	if (isTransactionInProgress == false) {
		wasDatabaseOpen = (database != NULL);
		connectToDatabase();
	}
	
	sqlite3_stmt *statement;
	sqlite3_prepare_v2(database, sql, -1, &statement, NULL);
	while (sqlite3_step(statement) == SQLITE_ROW) {
		// Build object data
		Concept *object = new Concept();
		object->populateEntity(statement);
		vector.push_back(object);
	}
	sqlite3_finalize(statement);
	
	if (isTransactionInProgress == false && wasDatabaseOpen == false) {
		closeConnectionToDatabase();
	}
	
	return vector; 
}

std::vector<ConceptInstance *> SQLiteDatabase::getConceptInstances(const char *sql) { 
	std::vector<ConceptInstance *> vector;
	
		bool wasDatabaseOpen = false;
		if (isTransactionInProgress == false) {
			wasDatabaseOpen = (database != NULL);
			connectToDatabase();
		}
		
		sqlite3_stmt *statement;
		sqlite3_prepare_v2(database, sql, -1, &statement, NULL);
		while (sqlite3_step(statement) == SQLITE_ROW) {
			// Build object data
			ConceptInstance *object = new ConceptInstance();
			object->populateEntity(statement);
			vector.push_back(object);
		}
		sqlite3_finalize(statement);
		
		if (isTransactionInProgress == false && wasDatabaseOpen == false) {
			closeConnectionToDatabase();
		}
	
	return vector; 
}

std::vector<ConceptLinker *> SQLiteDatabase::getConceptLinkers(const char *sql) { 
	std::vector<ConceptLinker *> vector;
	
		bool wasDatabaseOpen = false;
		if (isTransactionInProgress == false) {
			wasDatabaseOpen = (database != NULL);
			connectToDatabase();
		}
		
		sqlite3_stmt *statement;
		sqlite3_prepare_v2(database, sql, -1, &statement, NULL);
		while (sqlite3_step(statement) == SQLITE_ROW) {
			// Build object data
			ConceptLinker *object = new ConceptLinker();
			object->populateEntity(statement);
			vector.push_back(object);
		}
		sqlite3_finalize(statement);
		
		if (isTransactionInProgress == false && wasDatabaseOpen == false) {
			closeConnectionToDatabase();
		}
	
	return vector; 
}

std::vector<ConceptNaturalLanguageRepresenter *> SQLiteDatabase::getNaturalLanguageRepresenters(const char *sql) { 
	std::vector<ConceptNaturalLanguageRepresenter *> vector;
	
		bool wasDatabaseOpen = false;
		if (isTransactionInProgress == false) {
			wasDatabaseOpen = (database != NULL);
			connectToDatabase();
		}
		
		sqlite3_stmt *statement;
		sqlite3_prepare_v2(database, sql, -1, &statement, NULL);
		while (sqlite3_step(statement) == SQLITE_ROW) {
			// Build object data
			ConceptNaturalLanguageRepresenter *object = new ConceptNaturalLanguageRepresenter();
			object->populateEntity(statement);
			vector.push_back(object);
		}
		sqlite3_finalize(statement);
		
		if (isTransactionInProgress == false && wasDatabaseOpen == false) {
			closeConnectionToDatabase();
		}
	
	return vector; 
}

std::vector<ConceptNaturalLanguageRepresentationLinker *> SQLiteDatabase::getNaturalLanguageRepresentationLinkers(const char *sql) { 
	std::vector<ConceptNaturalLanguageRepresentationLinker *> vector;
	
		bool wasDatabaseOpen = false;
		if (isTransactionInProgress == false) {
			wasDatabaseOpen = (database != NULL);
			connectToDatabase();
		}
		
		sqlite3_stmt *statement;
		sqlite3_prepare_v2(database, sql, -1, &statement, NULL);
		while (sqlite3_step(statement) == SQLITE_ROW) {
			// Build object data
			ConceptNaturalLanguageRepresentationLinker *object = new ConceptNaturalLanguageRepresentationLinker();
			object->populateEntity(statement);
			vector.push_back(object);
		}
		sqlite3_finalize(statement);
		
		if (isTransactionInProgress == false && wasDatabaseOpen == false) {
			closeConnectionToDatabase();
		}
	
	return vector; 
}
