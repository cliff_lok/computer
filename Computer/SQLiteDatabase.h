//
//  SQLiteDatabase.h
//  Computer
//
//  Created by E-Liang Tan on 16/11/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#ifndef Computer_SQLManager_h
#define Computer_SQLManager_h

#include <sqlite3.h>
#include "Constants.h"
#include "Database.h"
#include "DatabaseEntity.h"

AI_NAMESPACE_BEGIN

class SQLiteDatabase : public Database {
	sqlite3 *database;
	const char *filePath;
	bool isTransactionInProgress;
public:
	SQLiteDatabase(const char *filePath);
	~SQLiteDatabase();
	
	// These functions only need to be called when writing to the database.
	// All functions will open and close the database automatically.
	// Write commands might benefit from speed improvements if transactions are used, so you might want to use them.
	void connectToDatabase();
	void closeConnectionToDatabase();
	void beginTransaction();
	void endTransaction(bool closeConnection);
	
	void updateSchema(const char *filePath);
	
	void performRequest(const char *sql, int (*callback)(void *, int, char **, char **), void *context, char **errorMessage); // Raw request
	// All the code below is VERY repetitive, being mere copy pasted stuff. The only difference is the class name. TODO: Find a way to merge them.
	std::vector<Concept *> getConcepts(const char *sql);
	std::vector<ConceptInstance *> getConceptInstances(const char *sql);
	std::vector<ConceptLinker *> getConceptLinkers(const char *sql);
	std::vector<ConceptNaturalLanguageRepresenter *> getNaturalLanguageRepresenters(const char *sql);
	std::vector<ConceptNaturalLanguageRepresentationLinker *> getNaturalLanguageRepresentationLinkers(const char *sql);
};

AI_NAMESPACE_END

#endif
