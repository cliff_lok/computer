//
//  SemanticMap.cpp
//  Computer
//
//  Created by E-Liang Tan on 16/11/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#include <iostream>
#include "Constants.h"
#include "SemanticMap.h"

AI_NAMESPACE_USE

SemanticMap::SemanticMap(SQLiteDatabase *db) {
	database = db;
}

void SemanticMap::cacheEntireDatabase() {
	getAllConcepts();
	getAllConceptInstances();
	getAllConceptLinkers();
	getAllNaturalLanguageRepresenters();
	getAllNaturalLanguageRepresentationLinkers();
}

void SemanticMap::addConceptsForString(U_ICU_NAMESPACE::UnicodeString string, std::string languageCode) {
	/*
	 
	 Word based languages: "Quarks are elementary particles."
	 1. Tokenize by whitespaces.
	 
	 Character based languages: "你们都是些傻瓜！"
	 1. Tokenize by characters.
	 
	 Shared:
	 2. Remove punctuations at the start or end of each token. Store locations of punctuation marks (token index, front/back).
	 3. Begin after last represented word with the format SELECT representations WHERE string REGEXP '\bI(\s)(really(\slike(\selementary(\sparticles)?)?)?)?\b'. Implement REGEXP http://www.sqlite.org/c3ref/create_function.html
	 4. Initialize representation objects from selected representation data.
	 5. If one or more representation object was selected:
	 5a. If one passes, push it into the main dump vector. 
	 5b. Else if more than one pass, add the optional tags one by one starting from the last word (according to the prototype regex specified in step 3), run the updated regex on the representations and run 5a2.
	 5c. Else if last word has not been reached and none pass, the previous combination was correct. If there was only one pass for the previous combination, push that into the main dump vector. If more than one, put the conflicting representations in a vector, which is put in a holding vector first and push a marker object into the dump vector.
	 5d. Otherwise word is new. Push marker into dump vector.
	 6. Insert punctuation markers into dump vector if appropriate.
	 7. Repeat step 3 - 6 until whole string is consumed.
	 8. For every representation in the dump vector, find the represented object and add them to the caches.
	 9. For every representation in the holding vector, find the represented object and add them to a second holding vector structured in the same way as the first.
	 10. For every conflict in the holding vector, for every concept in the conflict, branch out from the concept until contact is made with one of the concept or concept instance in the cache. Pick that concept, add it to the cache along with all the linkages.
	 11. Dump the holding vectors. We now have a working map.
	 
	 Markers mentioned:
	 - More than one pass conflict marker
	 - New word marker
	 - Punctuation marker
	 
	 Potential problems:
	 Unsolved: If a word has multiple meanings.
	 Unsolved: If all the essential words in sentence all have multiple meanings.
	 Solved (sort of): Different forms of a word/phrase. Solution: Each time-sensitive word has a time specifier of some sort (to be figured out, but I think they should simply link to the concept of "parts of speech", which are linked to words. "Parts of speech" would be linked to the time concepts like "now", "before" and "after" and so forth). Words of different parts of speech will be separate representations.
	 Solved: If phrase is encountered but not stored in database yet, phrase will be dumbed down to a word, which won't make much sense. Solution (lousy but should work): assimilate the language's dictionary before use, where there's a clear title. And assimilate Wikipedia after that for additional content. Or assimilate Wolphram|Alpha, but that might be harder.
	 
	 */
}

std::vector<Concept *> SemanticMap::getAllConcepts() {
	std::vector<Concept *> vector;
	std::vector<Concept *>::iterator vit;
	
	std::string sql = "SELECT * FROM concept";
//	if (concepts.size() > 0) {
//		sql += "WHERE concept_id NOT IN (";
//		for (vit = concepts.begin(); vit < concepts.end(); vit++) {
//			sql += (*vit)->conceptID;
//			if (vit != concepts.end()-1) {
//				sql += ",";
//			}
//		}
//		sql += ")";
//	}
//	
	vector = database->getConcepts(sql.c_str());
	return vector;
//	for (vit = vector.begin(); vit < vector.end(); vit++) {
//		concepts.push_back(*vit);
//	}
//	return concepts;
}

std::vector<ConceptInstance *> SemanticMap::getAllConceptInstances() {
	std::vector<ConceptInstance *> vector;
	std::vector<ConceptInstance *>::iterator vit;
	
	std::string sql = "SELECT * FROM concept_instance";
//	if (concepts.size() > 0) {
//		sql += "WHERE concept_instance_id NOT IN (";
//		for (vit = conceptInstances.begin(); vit < conceptInstances.end(); vit++) {
//			sql += (*vit)->conceptInstanceID;
//			if (vit != conceptInstances.end()-1) {
//				sql += ",";
//			}
//		}
//		sql += ")";
//	}
//	
	vector = database->getConceptInstances(sql.c_str());
	return vector;
//	for (vit = vector.begin(); vit < vector.end(); vit++) {
//		conceptInstances.push_back(*vit);
//	}
//	return conceptInstances;
}

std::vector<ConceptLinker *> SemanticMap::getAllConceptLinkers() {
	std::vector<ConceptLinker *> vector;
	std::vector<ConceptLinker *>::iterator vit;
	
	std::string sql = "SELECT * FROM concept_instance";
//	if (concepts.size() > 0) {
//		sql += "WHERE concept_instance_id NOT IN (";
//		for (vit = conceptLinkers.begin(); vit < conceptLinkers.end(); vit++) {
//			sql += (*vit)->linkerID;
//			if (vit != conceptLinkers.end()-1) {
//				sql += ",";
//			}
//		}
//		sql += ")";
//	}
//	
	vector = database->getConceptLinkers(sql.c_str());
	return vector;
//	for (vit = vector.begin(); vit < vector.end(); vit++) {
//		conceptLinkers.push_back(*vit);
//	}
//	return conceptLinkers;
}

std::vector<ConceptNaturalLanguageRepresenter *> SemanticMap::getAllNaturalLanguageRepresenters() {
	std::vector<ConceptNaturalLanguageRepresenter *> vector;
	std::vector<ConceptNaturalLanguageRepresenter *>::iterator vit;
	
	std::string sql = "SELECT * FROM concept_natural_language_representer";
//	if (concepts.size() > 0) {
//		sql += "WHERE representer_id NOT IN (";
//		for (vit = conceptNaturalLanguageRepresenters.begin(); vit < conceptNaturalLanguageRepresenters.end(); vit++) {
//			sql += (*vit)->representerID;
//			if (vit != conceptNaturalLanguageRepresenters.end()-1) {
//				sql += ",";
//			}
//		}
//		sql += ")";
//	}
//	
	vector = database->getNaturalLanguageRepresenters(sql.c_str());
	return vector;
//	for (vit = vector.begin(); vit < vector.end(); vit++) {
//		conceptNaturalLanguageRepresenters.push_back(*vit);
//	}
//	return conceptNaturalLanguageRepresenters;
}

std::vector<ConceptNaturalLanguageRepresentationLinker *> SemanticMap::getAllNaturalLanguageRepresentationLinkers() {
	std::vector<ConceptNaturalLanguageRepresentationLinker *> vector;
	std::vector<ConceptNaturalLanguageRepresentationLinker *>::iterator vit;
	
	std::string sql = "SELECT * FROM concept_natural_language_representer_linker";
//	if (concepts.size() > 0) {
//		sql += "WHERE representation_linker_id NOT IN (";
//		for (vit = conceptNaturalLanguageRepresentationLinkers.begin(); vit < conceptNaturalLanguageRepresentationLinkers.end(); vit++) {
//			sql += (*vit)->representationLinkerID;
//			if (vit != conceptNaturalLanguageRepresentationLinkers.end()-1) {
//				sql += ",";
//			}
//		}
//		sql += ")";
//	}
//	
	vector = database->getNaturalLanguageRepresentationLinkers(sql.c_str());
	return vector;
//	for (vit = vector.begin(); vit < vector.end(); vit++) {
//		conceptNaturalLanguageRepresentationLinkers.push_back(*vit);
//	}
//	return conceptNaturalLanguageRepresentationLinkers;
}

