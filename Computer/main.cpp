//
//  main.cpp
//  Computer
//
//  Created by E-Liang Tan on 7/7/11.
//  Copyright 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#include <iostream>
#include <sqlite3.h>
#include "Constants.h"
#include "Database.h"
#include "SQLiteDatabase.h"
#include "DatabaseEntity.h"
#include "SemanticMap.h"

/* 
 
 Start up sequence:
 Instantiate SQLiteDatabase
 SQLiteDatabase updateSchema()
 
 Main Run loop:
 
 Layer 1: Take in statement
 Layer 2.1: Analyze statement
 Layer 2.2: Instantiate concepts of focus, along with all linkers leading away from it. Information to be purged from caches written off to database, then purged.
 Layer 2.2: Add all information to semantic map
 
 Layer 2.1: If statement is a question:
 Layer 2.1: Pass statement to Logic Core.
 Layer 3: Pick out relavent concepts and linkers and put into map answering the question. Return map.
 Layer 2.1: From returned map, starting from concept of focus, branch out, for each concept and linker, pick out Natural Language Representers with the right language, closest meaning and connotation.
 Layer 1: Output Natural Language
 
 On other threads:
 Layer 3: Find potential implications of data, find conflicting data and purge it after confirmation. Things on interest to be passed to Layer 1 for research. Priority goes to new data.
 Layer 1: Search for more information requested by Layer 3 by querying human, or once a suitable intelligence level has been reached, the Internet.
 
 */

AI_NAMESPACE_USE

int main (int argc, const char * argv[]) {
	const char *filePathToSchemaSpecs = "/Users/Eliang/Documents/Developer/XcodeProjects/Computer/ai_schema_specification.db";
	const char *filePathToDatabase = "/Users/Eliang/Documents/Developer/XcodeProjects/Computer/ai_computer_database.db";
	
	SQLiteDatabase *database = new SQLiteDatabase(filePathToDatabase);
	
	sqlite3 *schemaSpecificationsDatabase;
	sqlite3_open(filePathToSchemaSpecs, &schemaSpecificationsDatabase);
	database->updateSchema(filePathToSchemaSpecs);
	sqlite3_close(schemaSpecificationsDatabase);
	
	std::cout << "Greetings lifeform. Computer here.\n\n";
	
	while (1) {
		std::cout << "Input: ";
		std::string input;
		std::cin >> input;
		
		if (input == ".quit") {
			break;
		}
		
		std::cout << "Received input: \"" + input + "\"\n";
		
		SemanticMap *semanticMap = new SemanticMap(database);
		delete semanticMap;
		
		std::string output = "";
		std::cout << "Output: \"" + output + "\"\n";
	}
	
	delete database;
	
	std::cout << "\nLive long and prosper. Computer out.\n";
    return 0;
}

