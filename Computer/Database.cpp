//
//  Database.cpp
//  Computer
//
//  Created by E-Liang Tan on 16/11/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#include <iostream>
#include "Database.h"

AI_NAMESPACE_USE

void Database::connectToDatabase() {}
void Database::closeConnectionToDatabase() {}

//std::vector<Concept *> Database::getAllConcepts() { std::vector<Concept *> vector; return vector; }
//std::vector<Concept *> Database::getConceptsWithIDs(int ids[]) { std::vector<Concept *> vector; return vector; }
//std::vector<ConceptInstance *> Database::getAllConceptInstances() { std::vector<ConceptInstance *> vector; return vector; };
//std::vector<ConceptInstance *> Database::getConceptInstancesWithIDs(int ids[]) { std::vector<ConceptInstance *> vector; return vector; };
//std::vector<ConceptInstance *> Database::getConceptInstancesWithInstantiatedConceptID(int conceptID) { std::vector<ConceptInstance *> vector; return vector; };
//std::vector<ConceptLinker *> Database::getAllConceptLinkers() { std::vector<ConceptLinker *> vector; return vector; };
//std::vector<ConceptLinker *> Database::getConceptLinkersWithIDs(int ids[]) { std::vector<ConceptLinker *> vector; return vector; };
//std::vector<ConceptLinker *> Database::getConceptLinkersWithOriginatingID(int conceptID, ConceptLinkerType linkerType) { std::vector<ConceptLinker *> vector; return vector; };
//std::vector<ConceptLinker *> Database::getConceptLinkersWithTargetID(int conceptID, ConceptLinkerType linkerType) { std::vector<ConceptLinker *> vector; return vector; };
//std::vector<ConceptLinker *> Database::getConceptLinkersWithRepresentedConceptID(int conceptID) { std::vector<ConceptLinker *> vector; return vector; };
void Database::getConceptsWithConceptOfFocus(Concept *conceptOfFocus, std::vector<Concept *> concepts, std::vector<ConceptInstance *> conceptInstances, std::vector<ConceptLinker *> conceptLinkers) {};

//std::vector<ConceptNaturalLanguageRepresenter *> Database::getNaturalLanguageRepresenters() { std::vector<ConceptNaturalLanguageRepresenter *> vector; return vector; };
//std::vector<ConceptNaturalLanguageRepresenter *> Database::getNaturalLanguageRepresentersWithIDs(int ids[]) { std::vector<ConceptNaturalLanguageRepresenter *> vector; return vector; };
//std::vector<ConceptNaturalLanguageRepresenter *> Database::getNaturalLanguageRepresentersWithLanguage(std::string languageCode) { std::vector<ConceptNaturalLanguageRepresenter *> vector; return vector; };
//std::vector<ConceptNaturalLanguageRepresenter *> Database::getNaturalLanguageRepresentersWithRoughPronunciation(U_ICU_NAMESPACE::UnicodeString roughPronunciation) { std::vector<ConceptNaturalLanguageRepresenter *> vector; return vector; };
//std::vector<ConceptNaturalLanguageRepresenter *> Database::getNaturalLanguageRepresentersWithString(U_ICU_NAMESPACE::UnicodeString string) { std::vector<ConceptNaturalLanguageRepresenter *> vector; return vector; };

//std::vector<ConceptNaturalLanguageRepresentationLinker *> Database::getNaturalLanguageRepresentationLinkers() { std::vector<ConceptNaturalLanguageRepresentationLinker *> vector; return vector; };
//std::vector<ConceptNaturalLanguageRepresentationLinker *> Database::getNaturalLanguageRepresentationLinkersWithIDs(int ids[]) { std::vector<ConceptNaturalLanguageRepresentationLinker *> vector; return vector; };
//std::vector<ConceptNaturalLanguageRepresentationLinker *> Database::getNaturalLanguageRepresentationLinkersWithNaturalLanguageRepresenterIDs(int ids[]) { std::vector<ConceptNaturalLanguageRepresentationLinker *> vector; return vector; };
//std::vector<ConceptNaturalLanguageRepresentationLinker *> Database::getNaturalLanguageRepresentationLinkersWithRepresentedIDs(int ids[], ConceptLinkerType linkerType) { std::vector<ConceptNaturalLanguageRepresentationLinker *> vector; return vector; };
