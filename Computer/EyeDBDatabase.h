//
//  EyeDBDatabase.h
//  Computer
//
//  Created by E-Liang Tan on 23/11/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//	
//	EyeDB is a NoSQL database, and uses OQL which is object-oriented, which is much more convenient than SQL.
//	However the project is a little too small for comfort, which might make support and stability difficult.
//	So I'll just leave this here while we consider the possibility of using this.
//	http://www.eyedb.org/
//

#ifndef Computer_EyeDBDatabase_h
#define Computer_EyeDBDatabase_h

//#include <eyedb/eyedb.h>
#include <dispatch/dispatch.h>
#include "Constants.h"
#include "Database.h"

AI_NAMESPACE_BEGIN

class EyeDBDatabase : public Database {
	
};

AI_NAMESPACE_END

#endif
