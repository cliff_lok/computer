//
//  Constants.h
//  Computer
//
//  Created by E-Liang Tan on 16/11/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#ifndef Computer_Constants_h
#define Computer_Constants_h

#include <unicode/uversion.h>
#include <unicode/uchar.h>
#include <unicode/unistr.h>

#define AI_NAMESPACE ai
#define AI_NAMESPACE_BEGIN extern "C++" { namespace AI_NAMESPACE {
#define AI_NAMESPACE_END } }
#define AI_NAMESPACE_USE using namespace AI_NAMESPACE;

AI_NAMESPACE_BEGIN

typedef int64_t		ai_int64;
typedef u_int64_t	ai_uint64;

// All #defines here
#define IS_USING_SQLITE3 1
#define IS_USING_STORE_XCODE 1

#if IS_USING_SQLITE3

#if _WIN32
// Import your SQLite 3 header here
#elif IS_USING_STORE_XCODE
#include <sqlite3.h>
#else
#include </usr/include/sqlite3.h>
#endif

typedef sqlite3_int64	ai_int64;
typedef sqlite3_uint64	ai_uint64;

#endif // IS_USING_SQLITE3

AI_NAMESPACE_END

#ifndef __BLOCKS__
#warning You need Blocks to be able to compile this program.
#endif

#endif // Computer_Constants_h
