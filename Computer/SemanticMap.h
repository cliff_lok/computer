//
//  SemanticMap.h
//  Computer
//
//  Created by E-Liang Tan on 16/11/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#ifndef Computer_SemanticMap_h
#define Computer_SemanticMap_h

#include "Constants.h"
//#include "Database.h"
#include "SQLiteDatabase.h"
#include "DatabaseEntity.h"

#include <vector>

AI_NAMESPACE_BEGIN

class SemanticMap {
	SQLiteDatabase *database;
public:
//	SemanticMap(Database *database);
	SemanticMap(SQLiteDatabase *database);
	void cacheEntireDatabase(); // This loads the entire database into memory. Not a good idea normally.
	
	// SCREW THE CACHES - Get something to work first before we use this. It's unnecessary complication.
	// These are caches - Caches only store objects directly linked related to subjects of focus, for quick access.
//	std::vector<Concept *> concepts;
//	std::vector<ConceptInstance *> conceptInstances;
//	std::vector<ConceptLinker *> conceptLinkers;
//	std::vector<ConceptNaturalLanguageRepresenter *> conceptNaturalLanguageRepresenters;
//	std::vector<ConceptNaturalLanguageRepresentationLinker *> conceptNaturalLanguageRepresentationLinkers;
	
	void addConceptsForString(U_ICU_NAMESPACE::UnicodeString string, std::string languageCode);
	
	// The map. This class should manage the fetching, always trying to use cached versions of data before fetching from database.
	std::vector<Concept *> getAllConcepts();
	std::vector<ConceptInstance *> getAllConceptInstances();
	std::vector<ConceptLinker *> getAllConceptLinkers();
	std::vector<ConceptNaturalLanguageRepresenter *> getAllNaturalLanguageRepresenters();
	std::vector<ConceptNaturalLanguageRepresentationLinker *> getAllNaturalLanguageRepresentationLinkers();
//	
//	std::vector<Concept *> getConceptsWithIDs(int ids[]);
//	std::vector<ConceptInstance *> getConceptInstancesWithIDs(int ids[]);
//	std::vector<ConceptInstance *> getConceptInstancesWithInstantiatedConceptID(int conceptID);
//	std::vector<ConceptLinker *> getConceptLinkersWithIDs(int ids[]);
//	std::vector<ConceptLinker *> getConceptLinkersWithOriginatingID(int conceptID, ConceptLinkerType linkerType);
//	std::vector<ConceptLinker *> getConceptLinkersWithTargetID(int conceptID, ConceptLinkerType linkerType);
//	std::vector<ConceptLinker *> getConceptLinkersWithRepresentedConceptID(int conceptID);
//	void getConceptsWithConceptOfFocus(Concept *conceptOfFocus, std::vector<Concept *> concepts, std::vector<ConceptInstance *> conceptInstances, std::vector<ConceptLinker *> conceptLinkers);
//	
//	std::vector<ConceptNaturalLanguageRepresenter *> getNaturalLanguageRepresentersWithIDs(int ids[]);
//	std::vector<ConceptNaturalLanguageRepresenter *> getNaturalLanguageRepresentersWithLanguage(std::string languageCode);
//	std::vector<ConceptNaturalLanguageRepresenter *> getNaturalLanguageRepresentersWithRoughPronunciation(U_ICU_NAMESPACE::UnicodeString roughPronunciation);
//	std::vector<ConceptNaturalLanguageRepresenter *> getNaturalLanguageRepresentersWithString(U_ICU_NAMESPACE::UnicodeString string);
//	
//	std::vector<ConceptNaturalLanguageRepresentationLinker *> getNaturalLanguageRepresentationLinkersWithIDs(int ids[]);
//	std::vector<ConceptNaturalLanguageRepresentationLinker *> getNaturalLanguageRepresentationLinkersWithNaturalLanguageRepresenterIDs(int ids[]);
//	std::vector<ConceptNaturalLanguageRepresentationLinker *> getNaturalLanguageRepresentationLinkersWithRepresentedIDs(int ids[], ConceptLinkerType linkerType);
};

AI_NAMESPACE_END

#endif
