//
//  RegularExpression.cpp
//  Computer
//
//  Created by E-Liang Tan on 29/11/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//
//	All the code below use <regex.h> by Henry Spencer, but it doesn't support Unicode so we can't use it.
//	PCRE also has a C++ API so that's awesome too.
//

#include <iostream>
#include <pcrecpp.h> // You need to have the PCRE library for this to work. And Unicode must be enabled. http://www.pcre.org/ http://www.regular-expressions.info/pcre.html
#include <cstring>
#include "RegularExpression.h"

//std::string *get_regerror(int errcode, regex_t *compiled);

AI_NAMESPACE_USE

//std::string *get_regerror (int errcode, regex_t *compiled) {
//	size_t length = regerror (errcode, compiled, NULL, 0);
//	char buffer[length];
//	regerror (errcode, compiled, buffer, length);
//	std::string *error = new std::string(buffer);
//	return error;
//}

bool RegularExpression::hasMatchesInString(const char *expression, const char *string, std::string **errorString) {
	return false;
	
//	regex_t *regex;
//	int errorCode = 0;
//	
//	errorCode = regcomp(regex, expression, REG_ICASE | REG_NOSUB);
//	if (errorCode != 0) {
//		*errorString = get_regerror(errorCode, regex);
//		regfree(regex);
//		return false;
//	}
//	
//	errorCode = regexec(regex, string, 0, NULL, 0);
//	if (errorCode != 0 && errorCode != REG_NOMATCH) {
//		*errorString = get_regerror(errorCode, regex);
//		regfree(regex);
//		return false;
//	}
//	
//	bool hasMatches = ((errorCode == 0) ? true : false);
//	return hasMatches;
}
