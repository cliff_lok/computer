//
//  AINeuralNetwork.h
//  Computer
//
//  Created by E-Liang Tan on 9/8/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#ifndef Computer_AINeuralNetwork_h
#define Computer_AINeuralNetwork_h

#include <vector>

class AINeuron {
public:
	
	AINeuron(int inputCount);
	
	int inputCount;
	std::vector<double> inputWeights; // Last object == threshold value
	
	void randomizeWeights(); // Call this after inputs has been set.
};

class AINeuronLayer {
	
	
	
public:
	
};

class AINeuralNet {
	
public:
	std::vector<AINeuronLayer> neuronLayers;
	
};

#endif
