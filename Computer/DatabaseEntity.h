//
//  DatabaseEntity.h
//  Computer
//
//  Created by E-Liang Tan on 21/11/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//	
//	Proposals (something to consider)
//	Proposal 1: Add concept linker group. This will group relatedconcept linkers together, sort of drawing a boundary around a few linkers. E.g. In "electron has charge equalto (-1)coulomb", has and equalto will be grouped. Potential problem: How to match one of these from natural language?
//	Proposal 2: Make concept linkers bare, make them a special thing with the meaning of a specified concept. Basically strip out all the built-in preset values. No potential problems other than some potential complication when interpreting natural language.
//	

#ifndef Computer_DatabaseEntity_h
#define Computer_DatabaseEntity_h

#include "Constants.h"
#include <unicode/unistr.h>
#include <vector>

AI_NAMESPACE_BEGIN

class Concept;
class ConceptInstance;
class ConceptLinker;
class ConceptNaturalLanguageRepresenter;
class ConceptNaturalLanguageRepresentationLinker;

class DatabaseEntity {
public:
	virtual void populateEntity(sqlite3_stmt *preparedStatement) {} // Populate instance variables of object. All columns are not guaranteed to exist.
	virtual const char *updateSQL() { return ""; } // SQL to update database values.
};

class Concept : public DatabaseEntity {
public:
	ai_uint64 conceptID;
	
	// Initializing
	void populateEntity(sqlite3_stmt *preparedStatement);
	const char *updateSQL();
};

class ConceptInstance : public DatabaseEntity {
public:
	ai_uint64 conceptInstanceID;
	ai_uint64 instantiatedConceptID;
	
	// Initializing
	void populateEntity(sqlite3_stmt *preparedStatement);
	const char *updateSQL();
};

enum {
	// Preset types, for convenience on learning stage, before enough knowledge is aquired to link by itself
	ConceptLinkIsEqualTo = 0, // = {Speed} IsEqualTo ({Distance} {MathOperatorDivide} {Time})
	ConceptLinkIsTypeOf = 1, // E.g. {Notebook} IsTypeOf {Book}. {MathOperatorDivide} IsTypeOf {Mathematical Operator}. {Quark} IsTypeOf {Elementary Particle}. {Proton} IsTypeOf {Subatomic Particle}
	ConceptLinkHasTarget = 2, // E.g. {Glass} HasTarget {Transparent}. {Proton} HasTarget 2{Up Quark} + {Down Quark} + {Electron}
	ConceptLinkContainsTarget = 3,
	
	// Arbitrarily decided by AI, can be anything
	ConceptLinkArbitrary = 1234
};
typedef ai_uint64 ConceptLink;

enum {
	ConceptLinkerToConcept,
	ConceptLinkerToConceptInstance,
	ConceptLinkerToLinker
};
typedef ai_uint64 ConceptLinkerType;

class ConceptLinker : public DatabaseEntity {
public:
	ai_uint64 linkerID;
	ConceptLink link;
	double certainty;
	
	// To identify the class at either end of the linker
	ConceptLinkerType originatingConceptLinkerEndpointClass; // Identifies originating class
	ConceptLinkerType targetConceptLinkerEndpointClass; // Identifies target class
	
	ai_uint64 originatingID;
	ai_uint64 targetID;
//	Concept *originatingConcept(); // Returns non-null if originatingConceptLinkerEndpointClass == ConceptLinkerToConcept
//	Concept *targetConcept(); // Returns non-null if targetConceptLinkerEndpointClass == ConceptLinkerToConcept
//	ConceptInstance *originatingConceptInstance(); // Returns non-null if originatingConceptLinkerEndpointClass == ConceptLinkerToConceptInstance
//	ConceptInstance *targetConceptInstance(); // Returns non-null if targetConceptLinkerEndpointClass == ConceptLinkerToConceptInstance
	
	double targetQuantity;
	// If linker groups are added, units aren't really necessary anymore.
	ai_uint64 targetQuantityUnitConceptID;
	Concept *targetQuantityUnitConcept; // kilograms, etc
	
	ai_uint64 representedConceptID; // Used when linkerType == ConceptLinkArbitrary
	
	// Initializing
	void populateEntity(sqlite3_stmt *preparedStatement);
	const char *updateSQL();
};

// A word/phrase/expression to represent the concept
class ConceptNaturalLanguageRepresenter : public DatabaseEntity {
public:
	ai_uint64 representerID;
	const unsigned char *meaningsString; // All meanings crammed into a string as stored in the database
	std::vector<std::vector<ConceptNaturalLanguageRepresentationLinker> > meanings; // Each meaning is a vector of representation linkers. The most suitable and related meaning to the overall context of the conversation or statement is chosen. Stored in database as "|" separated strings of comma separated linker IDs.
	U_ICU_NAMESPACE::UnicodeString pronunciation; // IPA letters
	U_ICU_NAMESPACE::UnicodeString string; // The word/phrase/expression itself
	std::string languageCode; // ISO 639-3 Code, or if unknown, assign 10 character UDID until language can be identified.
	
	// Initializing
	void populateEntity(sqlite3_stmt *preparedStatement);
	const char *updateSQL();
};

// Links 
class ConceptNaturalLanguageRepresentationLinker : public DatabaseEntity {
public:
	ai_uint64 representationLinkerID;
	
	ai_uint64 representedID;
	double strength; // 0-1. E.g. {Annoyed} - 0.5{Anger}, {Furious} - 0.9{Anger}
	// To identify the class at representation end of the linker
	ConceptLinkerType linkedEndpointClass;
	
	ai_uint64 parentRepresenterID;
//	ConceptNaturalLanguageRepresenter *parentRepresenter();
	
	// Initializing
	void populateEntity(sqlite3_stmt *preparedStatement);
	const char *updateSQL();
};

AI_NAMESPACE_END

#endif
