//
//  DatabaseEntity.cpp
//  Computer
//
//  Created by E-Liang Tan on 21/11/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#include <iostream>
#include <cstring>
#include "DatabaseEntity.h"

AI_NAMESPACE_USE

void Concept::populateEntity(sqlite3_stmt *preparedStatement) {
	int columnCount = sqlite3_column_count(preparedStatement);
	for (int i = 0; i < columnCount; i++) {
		const char *columnName = sqlite3_column_name(preparedStatement, i);
		if (strcmp(columnName, "concept_id") == 0) conceptID = sqlite3_column_int64(preparedStatement, i);
	}
}

const char *Concept::updateSQL() {
	return "";
}

void ConceptInstance::populateEntity(sqlite3_stmt *preparedStatement) {
	int columnCount = sqlite3_column_count(preparedStatement);
	for (int i = 0; i < columnCount; i++) {
		const char *columnName = sqlite3_column_name(preparedStatement, i);
		if (strcmp(columnName, "concept_instance_id") == 0) conceptInstanceID = sqlite3_column_int64(preparedStatement, i);
		if (strcmp(columnName, "instantiated_concept_id") == 0) instantiatedConceptID = sqlite3_column_int64(preparedStatement, i);
	}
}

const char *ConceptInstance::updateSQL() {
	return "";
}

void ConceptLinker::populateEntity(sqlite3_stmt *preparedStatement) {
	int columnCount = sqlite3_column_count(preparedStatement);
	for (int i = 0; i < columnCount; i++) {
		const char *columnName = sqlite3_column_name(preparedStatement, i);
		
		if (strcmp(columnName, "linker_id") == 0) linkerID = sqlite3_column_int64(preparedStatement, i);
		if (strcmp(columnName, "link") == 0) link = sqlite3_column_int64(preparedStatement, i);
		if (strcmp(columnName, "certainty") == 0) certainty = sqlite3_column_double(preparedStatement, i);
		
		if (strcmp(columnName, "originating_concept_linker_endpoint_class") == 0) originatingConceptLinkerEndpointClass = sqlite3_column_int64(preparedStatement, i);
		if (strcmp(columnName, "target_concept_linker_endpoint_class") == 0) targetConceptLinkerEndpointClass = sqlite3_column_int64(preparedStatement, i);
		
		if (strcmp(columnName, "originating_id") == 0) originatingID = sqlite3_column_int64(preparedStatement, i);
		if (strcmp(columnName, "target_id") == 0) targetID = sqlite3_column_int64(preparedStatement, i);
		
		if (strcmp(columnName, "target_quantity") == 0) targetQuantity = sqlite3_column_double(preparedStatement, i);
		if (strcmp(columnName, "target_quantity_unit_concept_id") == 0) targetQuantityUnitConceptID = sqlite3_column_int64(preparedStatement, i);
		if (strcmp(columnName, "represented_concept_id") == 0) representedConceptID = sqlite3_column_int64(preparedStatement, i);
	}
}

const char *ConceptLinker::updateSQL() {
	return "";
}

void ConceptNaturalLanguageRepresenter::populateEntity(sqlite3_stmt *preparedStatement) {
	int columnCount = sqlite3_column_count(preparedStatement);
	for (int i = 0; i < columnCount; i++) {
		const char *columnName = sqlite3_column_name(preparedStatement, i);
		if (strcmp(columnName, "representer_id") == 0) representerID = sqlite3_column_int64(preparedStatement, i);
		if (strcmp(columnName, "meanings") == 0) meaningsString = sqlite3_column_text(preparedStatement, i);
		if (strcmp(columnName, "pronunciation") == 0) pronunciation = U_ICU_NAMESPACE::UnicodeString((const char *)sqlite3_column_text(preparedStatement, i));
		if (strcmp(columnName, "string") == 0) string = U_ICU_NAMESPACE::UnicodeString((const char *)sqlite3_column_text(preparedStatement, i));
		if (strcmp(columnName, "language_code") == 0) languageCode = std::string((const char *)sqlite3_column_text(preparedStatement, i));
	}
}

const char *ConceptNaturalLanguageRepresenter::updateSQL() {
	return "";
}

void ConceptNaturalLanguageRepresentationLinker::populateEntity(sqlite3_stmt *preparedStatement) {
	int columnCount = sqlite3_column_count(preparedStatement);
	for (int i = 0; i < columnCount; i++) {
		const char *columnName = sqlite3_column_name(preparedStatement, i);
		
		if (strcmp(columnName, "representation_linker_id") == 0) representationLinkerID = sqlite3_column_int64(preparedStatement, i);
		
		if (strcmp(columnName, "represented_id") == 0) representedID = sqlite3_column_int64(preparedStatement, i);
		if (strcmp(columnName, "strength") == 0) strength = sqlite3_column_double(preparedStatement, i);
		if (strcmp(columnName, "linked_endpoint_class") == 0) linkedEndpointClass = sqlite3_column_int64(preparedStatement, i);
		
		if (strcmp(columnName, "parent_representer_id") == 0) parentRepresenterID = sqlite3_column_int64(preparedStatement, i);
	}
}

const char *ConceptNaturalLanguageRepresentationLinker::updateSQL() {
	return "";
}
