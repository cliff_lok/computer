//
//  AINeuralNetwork.cpp
//  Computer
//
//  Created by E-Liang Tan on 9/8/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#include <iostream>
#include <ctime>
#include <cstdlib>
#include "AINeuralNetwork.h"

using namespace std;

AINeuron::AINeuron(int numInput) {
	inputCount = numInput;
	
	srand((unsigned int)time(NULL));
	
	for (int i = 0; i < inputCount+1; i++) {
		inputWeights.push_back(rand() % 1);
	}

}

//int AINeuron::getOutput() {
//	// x1w1 + x2w2 ... xnwn >= t
//	
//	if (hasEvaluatedOutput == true) {
//		return output;
//	}
//	
//	unsigned long inputCount = inputs.size();
//	double activation = 0;
//	
//	for (int i = 0; i < inputCount; i++) {
//		activation += (inputs[i].getOutput() * inputWeights[i]);
//	}
//	
//	output = ((activation >= inputWeights[inputCount]) ? 1 : 0);
//	hasEvaluatedOutput = true;
//	
//	return output;
//}
