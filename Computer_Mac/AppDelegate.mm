//
//  AppDelegate.mm
//  Computer
//
//  Created by E-Liang Tan on 23/11/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#import "AppDelegate.h"
#include <sqlite3.h>
#include <iostream>

AI_NAMESPACE_USE

@implementation AppDelegate

@synthesize database = _database;
@synthesize window = _window;
@synthesize chatButton = _chatButton;
@synthesize semanticMapEditor = _semanticMapEditor;
@synthesize semanticMapButton = _semanticMapButton;
@synthesize sqlButton = _sqlButton;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	// Insert code here to initialize your application
	NSString *pathToSchemaDB = [[NSBundle mainBundle] pathForResource:@"ai_schema_specification" ofType:@"db"];
	const char *filePathToSchemaSpecs = [pathToSchemaDB UTF8String];
//	const char *filePathToSchemaSpecs = "/Users/Eliang/Documents/Developer/XcodeProjects/Computer/ai_schema_specification.db";
	const char *filePathToDatabase = "/Users/Eliang/Documents/Developer/XcodeProjects/Computer/ai_computer_database.db";
	
	self.database = new SQLiteDatabase(filePathToDatabase);
	self.database->updateSchema(filePathToSchemaSpecs);
	
	[self showSemanticMapWindow:self];
	
	{ // This is the random testing area
//		std::vector<Concept *> vector;
//		vector = self.database->getConcepts("SELECT * FROM concept");
//		std::vector<Concept *>::iterator vit;
//		for (vit = vector.begin(); vit < vector.end(); vit++) {
//			NSLog(@"Vvalue: %llu", (*vit)->conceptID);
//		}
		
//		SemanticMap *map = new SemanticMap(self.database);
//		map->getAllConcepts();
//		map->getAllConcepts();
//		delete map;
	}
}

- (IBAction)showMainWindow:(id)sender {
	[self.window makeKeyAndOrderFront:self];
}

- (SemanticMapEditorWindowController *)semanticMapEditor {
	if (!_semanticMapEditor) {
		_semanticMapEditor = [[SemanticMapEditorWindowController alloc] initWithWindowNibName:@"SemanticMapEditorWindowController"];
	}
	return _semanticMapEditor;
}

- (IBAction)showChatWindow:(id)sender {
}

- (IBAction)showSemanticMapWindow:(id)sender {
	[self.semanticMapEditor showWindow:self];
}

- (IBAction)showSQLWindow:(id)sender {
}

@end
