//
//  AppDelegate.h
//  Computer
//
//  Created by E-Liang Tan on 23/11/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#include "Constants.h"
#include "Database.h"
#include "SQLiteDatabase.h"
#include "SemanticMap.h"
#include "DatabaseEntity.h"

#import "SemanticMapEditorWindowController.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property AI_NAMESPACE::SQLiteDatabase *database;
@property (assign) IBOutlet NSWindow *window;
- (IBAction)showMainWindow:(id)sender;

@property (weak) IBOutlet NSButton *chatButton;
- (IBAction)showChatWindow:(id)sender;

@property (weak) IBOutlet NSButton *semanticMapButton;
@property (strong, nonatomic) SemanticMapEditorWindowController *semanticMapEditor;
- (IBAction)showSemanticMapWindow:(id)sender;

@property (weak) IBOutlet NSButton *sqlButton;
- (IBAction)showSQLWindow:(id)sender;

@end
