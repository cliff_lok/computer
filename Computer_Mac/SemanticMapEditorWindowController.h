//
//  SemanticMapEditorWindowController.h
//  Computer
//
//  Created by E-Liang Tan on 8/12/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#include "SQLiteDatabase.h"
#include "SemanticMap.h"
#import "SemanticMapView.h"

@interface SemanticMapEditorWindowController : NSWindowController

- (id)initWithDatabase:(AI_NAMESPACE::SQLiteDatabase *)database;

@property AI_NAMESPACE::SemanticMap *semanticMap;
@property (weak) IBOutlet SemanticMapView *semanticMapView;

@property (weak) IBOutlet NSButton *addConceptButton;
- (IBAction)addConcept:(id)sender;

@property (weak) IBOutlet NSButton *addLinkButton;
- (IBAction)addLink:(id)sender;

@end
