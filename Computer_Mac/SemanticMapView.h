//
//  SemanticMapView.h
//  Computer
//
//  Created by E-Liang Tan on 8/12/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>

#include "Constants.h"
#include "SemanticMap.h"
#include "DatabaseEntity.h"

/*
 
 SemanticMapView
 
 This the UI representation of the SemanticMap, enabling user friendly, direct access to Computer's data.
 
 The underlying concepts and concept instances should be abstracted away, showing only the natural language parts. They can be differentiated by color or something.
 Reason being that every concept must have a concept, and every word that links to multiple concepts has its own concept, which can be specified by the concept linkers.
 ^ I don't think every word will link to a single concept, some link to two and is not a specific concept by itself. E.g. frustration = anger + other stuff
 Concept linkers are shown as lines with descriptions on them specifying the quantity and stuff.
 
 It should be an infinitely scrolling view, loading and removing concepts as person scrolls along. 
 Because to display everything you potentially need a multidimensional map, otherwise it will get very messy.
 
 Color coding:
 Concepts: Green
 Concept Instances: Blue
 Floating NLRs: Red
 Links: Create colors based on the RGB thing. So a linker linking NLRs to Instances would be RGB(255,0,255).
 
 */

@interface SemanticMapView : NSScrollView

@property (nonatomic) AI_NAMESPACE::SemanticMap *map;

@end

@interface SemanticMapConceptView : NSView // Concepts, Concept Instances. This view should show all natural language representers associated with it, separated by new lines. 

//- (id)initWithSemanticMapView:(SemanticMapView *)mapView 
//					  concept:(AI_NAMESPACE::Concept *)concept 
//			  conceptInstance:(AI_NAMESPACE::ConceptInstance *)conceptInstance 
//   naturalLanguageRepresenter:(AI_NAMESPACE::ConceptNaturalLanguageRepresenter *)nlr 
//  numberOfConceptsOnSameLevel:(ai_int64)numberOfConcepts 
//	indexOfThisConceptOnLevel:(ai_int64)index;
//
//+ (NSRect)conceptRectForConceptString:(NSString *)conceptString
//		  numberOfConceptsOnSameLevel:(ai_int64)numberOfConcepts 
//			indexOfThisConceptOnLevel:(ai_int64)index;

//@property AI_NAMESPACE::Concept *concept;
//@property AI_NAMESPACE::ConceptInstance *conceptInstance;
//- (NSString *)conceptString;

@end

@interface SemanticMapConceptLinkerView : NSView // Concept Linkers

- (id)initWithSemanticMapView:(SemanticMapView *)mapView 
	   originatingConceptView:(SemanticMapConceptView *)originator 
			targetConceptView:(SemanticMapConceptView *)target
				conceptLinker:(AI_NAMESPACE::ConceptLinker *)linker;

//@property AI_NAMESPACE::ConceptLinker *linker;
@property (strong, nonatomic) SemanticMapView *mapView;
@property (strong, nonatomic) SemanticMapConceptView *originator;
@property (strong, nonatomic) SemanticMapConceptView *target;

+ (NSRect)linkerRectForOriginatingConceptView:(SemanticMapConceptView *)originator 
							targetConceptView:(SemanticMapConceptView *)target 
									lineWidth:(CGFloat)lineWidth;

- (NSInteger)linkerLineGradient;

@end
