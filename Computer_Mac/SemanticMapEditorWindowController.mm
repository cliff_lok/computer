//
//  SemanticMapEditorWindowController.mm
//  Computer
//
//  Created by E-Liang Tan on 8/12/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#import "SemanticMapEditorWindowController.h"

@implementation SemanticMapEditorWindowController
@synthesize semanticMap = _semanticMap;
@synthesize addLinkButton;
@synthesize semanticMapView;
@synthesize addConceptButton;

- (id)initWithDatabase:(AI_NAMESPACE::SQLiteDatabase *)database {
	self = [super initWithWindowNibName:@"SemanticMapEditorWindowController"];
    if (self) {
		self.semanticMap = new AI_NAMESPACE::SemanticMap(database);
	}
	
	return self;
}

//- (id)initWithWindow:(NSWindow *)window
//{
//    self = [super initWithWindow:window];
//    if (self) {
//        // Initialization code here.
//		NSString *pathToSchemaDB = [[NSBundle mainBundle] pathForResource:@"ai_schema_specification" ofType:@"db"];
//		const char *filePathToSchemaSpecs = [pathToSchemaDB UTF8String];
//	//	const char *filePathToSchemaSpecs = "/Users/Eliang/Documents/Developer/XcodeProjects/Computer/ai_schema_specification.db";
//		const char *filePathToDatabase = "/Users/Eliang/Documents/Developer/XcodeProjects/Computer/ai_computer_database.db";
//		
//		self.database = new SQLiteDatabase(filePathToDatabase);
//		self.database->updateSchema(filePathToSchemaSpecs);
//    }
//    
//    return self;
//}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (IBAction)addConcept:(id)sender {
}
- (IBAction)addLink:(id)sender {
}
@end
