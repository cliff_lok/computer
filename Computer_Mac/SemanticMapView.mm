//
//  SemanticMapView.mm
//  Computer
//
//  Created by E-Liang Tan on 8/12/11.
//  Copyright (c) 2011 E-Liang Tan, Eliot Lim. All rights reserved.
//

#import <math.h>
#import "SemanticMapView.h"

AI_NAMESPACE_USE

@implementation SemanticMapView

@synthesize map = _map;

@end

@implementation SemanticMapConceptView

//- (id)initWithSemanticMapView:(SemanticMapView *)mapView 
//					  concept:(AI_NAMESPACE::Concept *)concept 
//			  conceptInstance:(AI_NAMESPACE::ConceptInstance *)conceptInstance 
//   naturalLanguageRepresenter:(AI_NAMESPACE::ConceptNaturalLanguageRepresenter *)nlr
//  numberOfConceptsOnSameLevel:(ai_int64)numberOfConcepts
//	indexOfThisConceptOnLevel:(ai_int64)index {
//	self = [super initWithFrame:<#(NSRect)#>];
//}

@end

@implementation SemanticMapConceptLinkerView

@synthesize mapView = _mapView;
@synthesize originator = _originator;
@synthesize target = _target;

- (id)initWithSemanticMapView:(SemanticMapView *)mapView 
	   originatingConceptView:(SemanticMapConceptView *)originator 
			targetConceptView:(SemanticMapConceptView *)target
				conceptLinker:(AI_NAMESPACE::ConceptLinker *)linker {
	self = [super initWithFrame:[SemanticMapConceptLinkerView linkerRectForOriginatingConceptView:originator targetConceptView:target lineWidth:5.0]];
	
	if (self) {
		self.mapView = mapView;
		self.originator = originator;
		self.target = target;
	}
	
	return self;
}

+ (NSRect)linkerRectForOriginatingConceptView:(SemanticMapConceptView *)originator 
							targetConceptView:(SemanticMapConceptView *)target 
									lineWidth:(CGFloat)lineWidth {
	NSPoint originatorCenter = NSMakePoint(originator.frame.origin.x + (originator.bounds.size.width/2), originator.frame.origin.y + (originator.bounds.size.height/2));
	NSPoint targetCenter = NSMakePoint(target.frame.origin.x + (target.bounds.size.width/2), target.frame.origin.y + (target.bounds.size.height/2));
	NSRect linkerRect;
	linkerRect.origin = NSMakePoint(fminf(originatorCenter.x, targetCenter.x), fminf(originatorCenter.y, targetCenter.y));
	linkerRect.size = NSMakeSize(fabsf(originator.frame.origin.x - target.frame.origin.x), fabsf(originator.frame.origin.y - target.frame.origin.y));
	return linkerRect;
}

- (NSInteger)linkerLineGradient {
	NSPoint originatorCenter = NSMakePoint(self.originator.frame.origin.x + (self.originator.bounds.size.width/2), self.originator.frame.origin.y + (self.originator.bounds.size.height/2));
	NSPoint targetCenter = NSMakePoint(self.target.frame.origin.x + (self.target.bounds.size.width/2), self.target.frame.origin.y + (self.target.bounds.size.height/2));
	if (originatorCenter.y == targetCenter.y) return 0;
	if (originatorCenter.x == targetCenter.x) return NSIntegerMax;
	if (originatorCenter.x < targetCenter.x && originatorCenter.y < targetCenter.y) return 1;
	if (originatorCenter.x > targetCenter.x && originatorCenter.y > targetCenter.y) return 1;
	if (originatorCenter.x < targetCenter.x && originatorCenter.y > targetCenter.y) return -1;
	if (originatorCenter.x > targetCenter.x && originatorCenter.y < targetCenter.y) return -1;
	return NSIntegerMin;
}

// TODO: Show the text boxes and stuff. Currently only draws a nice line between 2 concept views.

- (void)drawRect:(NSRect)dirtyRect {
	[super drawRect:dirtyRect];
	NSBezierPath *path = [NSBezierPath bezierPath];
	// Find the corners and draw a line between them.
	NSInteger gradient = [self linkerLineGradient];
	if (gradient == 1) {
		[path moveToPoint:NSMakePoint(0, self.bounds.size.height)];
		[path lineToPoint:NSMakePoint(self.bounds.size.width, 0)];
	}
	else if (gradient == -1) {
		[path moveToPoint:NSZeroPoint];
		[path lineToPoint:NSMakePoint(self.bounds.size.width, self.bounds.size.height)];
	}
	else if (gradient == 0) {
		[path moveToPoint:NSMakePoint(0, self.bounds.size.height / 2)];
		[path lineToPoint:NSMakePoint(self.bounds.size.width, self.bounds.size.height / 2)];
	}
	else if (gradient == NSIntegerMax) {
		[path moveToPoint:NSMakePoint(self.bounds.size.width / 2, 0)];
		[path lineToPoint:NSMakePoint(self.bounds.size.width / 2, self.bounds.size.height)];
	}
	else if (gradient == NSIntegerMin) {
		return;
	}
	
//	Concepts: Green
//	Concept Instances: Blue
//	Floating NLRs: Red
	
	CGFloat red = 0;
	CGFloat green = 0;
	CGFloat blue = 0;
	
	[[NSColor colorWithDeviceRed:red green:green blue:blue alpha:1.0] set];
	[path setLineWidth:5.0];
	[path setLineCapStyle:NSRoundLineCapStyle];
	[path stroke];
}

@end
